English|[简体中文](./README.CN.md)  
## DGLibrary is the SDK of DreamGame Technology's game products, which is used by platform providers to connect with our games.

## The SDK includes DG Baccarat, Dragon Tiger, Niu Niu, Roulette, Sic Bo, Color Disc, and Golden Flower Games

### Docking method 
	(1)Currently the SDK exists in the plugin project in order to reduce some unnecessary troubles. 
	(2)Integrate the host project into your own project (referred to as the host project), package the plugin project (plug-in project) into an apk, and place it in your own remote server for the host project to download and update dynamically. 
    

### Document change log

Version number | Modified content | Modified date
----|------|----
1.0.0 | 1.sdk plug-in    | 2022-07-01
1.0.1 | 1.sdk update video    | 2023-1-11
 
### Environment configuration
##### host project
  1. Add dependencies in host_demo\build.gradle
   			classpath 'com.android.tools.build:gradle:3.6.0'
        classpath classpathExt['replugin-host-gradle']
  
  2. app\build.gradle
	 1. Add dependencies  implementation dependenciesExt['replugin-host-library']
     1. Add plugin configuration information above dependencies, not at the beginning of this file    
		```
			apply plugin: 'replugin-host-gradle'
		  repluginHostConfig { 
    	  useAppCompat = false
   	    screenOrientation = 'landscape' 
		 }
		```
	 1.  add in android node 
		```
			dataBinding {
					enabled = true
			}
		```
 3. Application has some custom configuration
 4. Add permission
```
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
  
    <uses-permission android:name="android.permission.READ_SYNC_SETTINGS"/>
    <uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS"/>
    <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
    <uses-permission android:name="android.permission.READ_PHONE_STATE"/>
```
##### plugin project 
 1. Add dependencies in plugin_demo\build.gradle 
  			classpath classpathExt['androidx-build-tools']
        classpath classpathExt['replugin-plugin-gradle']
   
 2. app\build.gradle  
	1. Add dependencies  implementation dependenciesExt['replugin-host-library']
	1. Add plugin information at the bottom
		```
		 
		apply plugin: 'replugin-plugin-gradle'
			repluginPluginConfig {
    	pluginName = "demo1"  /*  The plugin alias is defined by yourself*/
    	hostApplicationId = "com.ca.dg.host"   /*  host   application id*/
    	hostAppLauncherActivity = "com.ca.dg.host.StartActivity"  /*  Host   startup class*/
		}
		```
 3. Define aliases in AndroidManifest.xml  
	```
				  <meta-data
            android:name="process_map"
            android:value="[
            {'from':'com.qihoo360.replugin.sample.demo1:bg','to':'$p0'}
            ]" />

        <meta-data
            android:name="com.qihoo360.plugin.name"
            android:value="demo1" />
	```		
 4. Add permission  
	```
					<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
		<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
		<uses-permission android:name="android.permission.INTERNET"/>
		<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
		<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
		<uses-permission android:name="android.permission.READ_SYNC_SETTINGS"/>
		<uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS"/>
		<uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
		<uses-permission android:name="android.permission.VIBRATE"/>
		<uses-permission android:name="android.permission.READ_PHONE_STATE"/>
	```
	
 5.To package this plugin project, remember to check V1 V2 when packaging.
 
 
##### Release
1. Encrypt the packaged plug-in apk (required, HostUtil), and change the way to obtain the plug-in apk to download,
Then decrypt the installation in MainActivity, and call it successfully in StartActivity.
	```
       
 				Intent intent = new Intent();
        // An alias is defined in the plugin's AndroidManifest.xml, which must be started as an alias
        intent.setComponent(new ComponentName(PluginConstant.pluginAlias, "com.ca.dg.activity.BridgeActivity"));
          intent.putExtra("minBet", 10);
        intent.putExtra("token", LoginUtil.token);
        intent.putExtra("gameId", gameId);
        intent.putExtra("language", 3);
        intent.putExtra("domains", LoginUtil.getDomains());
        intent.putExtra("hideBarrage", false);

        if (PluginConstant.getInstance().isPluginNeedStart()) {
            Toast.makeText(StartActivity.this, "app need restart", Toast.LENGTH_SHORT).show();
        } else {
            boolean startFlg = RePlugin.startActivityForResult(StartActivity.this, intent, REQUEST_CODE_DEMO5);
            if (!startFlg) {
                Toast.makeText(StartActivity.this, "start game fail", Toast.LENGTH_SHORT).show();
                boolean flg = RePlugin.uninstall(PluginConstant.pluginAlias);
                PluginConstant.getInstance().setPluginNeedStart();
            }
        }
	```  
 
2. About uninstalling the plug-in: as long as you enter the game or just install the plug-in, then uninstalling it will not take effect immediately, it will uninstall itself when you enter again. If the plug-in is installed before, and the app is re-entered without running the plug-in, the uninstallation can take effect immediately.  

3. Regarding plugin aliases, please keep the same in AndroidManifest.xml and build.gradle of plugin project and PluginConstant.pluginAlias ​​of host project in these three places.

##### Hint

android version | gradle | gradle plugin
----|------|----
+| gradle-6.1.1-bin.zip   | 3.6.0 