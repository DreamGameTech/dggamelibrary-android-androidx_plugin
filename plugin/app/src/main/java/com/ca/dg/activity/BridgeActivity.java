package com.ca.dg.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.ca.dg.app.AppVariable;

public class BridgeActivity extends Activity {

    private int result_code = 0x12;

    private int request_code = 0x11;

    public static int ParamError = 3;

    Intent intent1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        intent1 = getIntent();

        Intent intent = new Intent(this, DGLoadActivity.class);
        intent.putExtra("minBet", intent1.getIntExtra("minBet", 0));

        //1 中文
        //3 英文

//          language = 0; //默认3 英语
//        int CN = 1;
//        int HK = 2;
//         int EN = 3;
//         int KO = 4;  //韩国
//         int ID = 5;  //印尼
//          int TH = 6; // 泰国
//         int MY = 7; //缅甸
//          int VI = 8; //越南
        int language = intent1.getIntExtra("language", 1);
        AppVariable.changeLanguage(language);

        String token = intent1.getStringExtra("token");
        if (token == null || "".equals(token) || "undefined".equals(token)) {

            intent1.putExtra("reason", ParamError);

            intent1.putExtra("loadFlg", false);

            intent1.putExtra("onExit", false);

            setResult(result_code, intent);

            finish();
            return;
        }
        intent.putExtra("token", token);

        intent.putExtra("gameId", intent1.getIntExtra("gameId", 1));

        intent.putExtra("domains", intent1.getStringExtra("domains"));

        intent.putExtra("hideBarrage", intent1.getBooleanExtra("hideBarrage", false));

        startActivityForResult(intent, request_code);
    }

    /**
     * reason {
     * 默认: -1
     *  0:  socket 连接异常
     *  1: 会员初始化失败
     *  2 :账号停用
     *  3: 参数错误 ,比如domains 的参数异常
     * }
     *
     * loadFlg : 是否进入大厅成功 
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == result_code && request_code == requestCode) {

            boolean loadFlg = data.getBooleanExtra("loadFlg", false);

            int reason = data.getIntExtra("reason", -1);

            intent1.putExtra("loadFlg", loadFlg);

            intent1.putExtra("reason", reason);

            setResult(result_code, intent1);

            finish();
        }
    }
}

