package com.ca.dg.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.ca.dg.constant.GameId;
import com.ca.dg.util.UIHelper;
import com.qihoo360.replugin.sample.demo1.R;

import org.json.JSONObject;

/**
 * 描述：
 * Created by Aiden on 2020/1/7
 */
public class DemoActivity extends Activity implements View.OnClickListener {

    String token = null;
    String gameToken_URL = "https://login.ruihejade.com/apidata/game/h5";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_waike);
        findViewById(R.id.baccarat).setOnClickListener(this::onClick);
        findViewById(R.id.dragon).setOnClickListener(this::onClick);
        findViewById(R.id.token).setOnClickListener(this::onClick);

        findViewById(R.id.cn).setOnClickListener(this::onClick);
        findViewById(R.id.en).setOnClickListener(this::onClick);
        findViewById(R.id.th).setOnClickListener(this::onClick);
        findViewById(R.id.KO).setOnClickListener(this::onClick);

        findViewById(R.id.bull).setOnClickListener(this::onClick);
        findViewById(R.id.roullet).setOnClickListener(this::onClick);
        findViewById(R.id.sicbo).setOnClickListener(this::onClick);
        findViewById(R.id.sedie).setOnClickListener(this::onClick);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.baccarat:
                if (checkToken()) {
                    // 之前分 DGLoadUtil与DGLoad 因为一个要回调一个不要回调，现在都要回调就合并成同一个了
                    // DGLoadUtil.loadDGGameNoLoading();
                    // DGLoadUtil.loadDGGame();
                    // 该类为入口类，有两个方法，一个没有加载框，一个自带加载框
                    // 参数都一样
                    /*
                     * context 当前activity的contenxt
                     * token DG游戏token
                     * gameId  之前是枚举类型，不可能有其他值，现在是int类型，要注意传入的值，如果不是龙虎和牛牛都显示百家乐
                     * listener  DG游戏的回调
                     * */
                    if (!UIHelper.isSingle3()) {
                        break;
                    }
                    runOnUiThread(() -> {
                        // 之前分 DGLoadUtil与DGLoad 因为一个要回调一个不要回调，现在都要回调就合并成同一个了
                        // DGLoadUtil.loadDGGameNoLoading();
                        // DGLoadUtil.loadDGGame();
                        // 该类为入口类，有两个方法，一个没有加载框，一个自带加载框
                        // 参数都一样
                        /*
                         * context 当前activity的contenxt
                         * token DG游戏token （5分钟有效期）
                         * gameId  之前是枚举类型，不可能有其他值，现在是int类型，要注意传入的值，如果不是龙虎和牛牛都显示百家乐
                         * listener  DG游戏的回调
                         * */
                        DGLoadUtil.loadDGGame(getBaseContext(), token, GameId.BACCARAT, getDomains(), new DGDataListener() {
                            // 数据加载完成，还没有进入activity的回调
                            @Override
                            public void onLoadFinish() {

                            }

                            // 数据加载失败的回调，i 有三个类型
                            // SocketNetError = 0;    socket链接失败
                            // MemberInitError = 1;   会员初始化失败
                            // MemberStop = 2;        会员账号暂停
                            @Override
                            public void onLoadError(int i) {

                            }

                            // 进入DG Activity的回调
                            @Override
                            public void onEnter() {

                            }

                            // 退出SDK的回调
                            @Override
                            public void onExit() {

                            }

                        }, 0);
                    });

                }
                break;
//            case R.id.bull:
//                if (checkToken()) {
//                    if (!UIHelper.isSingle3()) {
//                        break;
//                    }
//                    // 之前分 DGLoadUtil与DGLoad 因为一个要回调一个不要回调，现在都要回调就合并成同一个了
//                    // DGLoadUtil.loadDGGameNoLoading();
//                    // DGLoadUtil.loadDGGame();
//                    // 该类为入口类，有两个方法，一个没有加载框，一个自带加载框
//                    // 参数都一样
//                    /*
//                     * context 当前activity的contenxt
//                     * token DG游戏token
//                     * gameId  之前是枚举类型，不可能有其他值，现在是int类型，要注意传入的值，如果不是龙虎和牛牛都显示百家乐
//                     * listener  DG游戏的回调
//                     * */
//                    runOnUiThread(() -> {
//                        // 之前分 DGLoadUtil与DGLoad 因为一个要回调一个不要回调，现在都要回调就合并成同一个了
//                        // DGLoadUtil.loadDGGameNoLoading();
//                        // DGLoadUtil.loadDGGame();
//                        // 该类为入口类，有两个方法，一个没有加载框，一个自带加载框
//                        // 参数都一样
//                        /*
//                         * context 当前activity的contenxt
//                         * token DG游戏token
//                         * gameId  之前是枚举类型，不可能有其他值，现在是int类型，要注意传入的值，如果不是龙虎和牛牛都显示百家乐
//                         * listener  DG游戏的回调
//                         * */
//                        DGLoadUtil.loadDGGame(DemoActivity.this, token, GameId.BULL, getDomains(), new DGDataListener() {
//                            // 数据加载完成，还没有进入activity的回调
//                            @Override
//                            public void onLoadFinish() {
//
//                            }
//
//                            // 数据加载失败的回调，i 有三个类型
//                            // SocketNetError = 0;    socket链接失败
//                            // MemberInitError = 1;   会员初始化失败
//                            // MemberStop = 2;        会员账号暂停
//                            @Override
//                            public void onLoadError(int i) {
//
//                            }
//
//                            // 进入DG Activity的回调
//                            @Override
//                            public void onEnter() {
//
//                            }
//
//                            // 退出SDK的回调
//                            @Override
//                            public void onExit() {
//
//                            }
//
//                        }, 0);
//                    });
//                }
//                break;
//            case R.id.roullet:
//                if (checkToken()) {
//                    if (!UIHelper.isSingle3()) {
//                        break;
//                    }
//                    // 之前分 DGLoadUtil与DGLoad 因为一个要回调一个不要回调，现在都要回调就合并成同一个了
//                    // DGLoadUtil.loadDGGameNoLoading();
//                    // DGLoadUtil.loadDGGame();
//                    // 该类为入口类，有两个方法，一个没有加载框，一个自带加载框
//                    // 参数都一样
//                    /*
//                     * context 当前activity的contenxt
//                     * token DG游戏token
//                     * gameId  之前是枚举类型，不可能有其他值，现在是int类型，要注意传入的值，如果不是龙虎和牛牛都显示百家乐
//                     * listener  DG游戏的回调
//                     * */
//                    runOnUiThread(() -> {
//                        // 之前分 DGLoadUtil与DGLoad 因为一个要回调一个不要回调，现在都要回调就合并成同一个了
//                        // DGLoadUtil.loadDGGameNoLoading();
//                        // DGLoadUtil.loadDGGame();
//                        // 该类为入口类，有两个方法，一个没有加载框，一个自带加载框
//                        // 参数都一样
//                        /*
//                         * context 当前activity的contenxt
//                         * token DG游戏token
//                         * gameId  之前是枚举类型，不可能有其他值，现在是int类型，要注意传入的值，如果不是龙虎和牛牛都显示百家乐
//                         * listener  DG游戏的回调
//                         * */
//                        DGLoadUtil.loadDGGame(DemoActivity.this, token, GameId.ROULETTE, getDomains(), new DGDataListener() {
//                            // 数据加载完成，还没有进入activity的回调
//                            @Override
//                            public void onLoadFinish() {
//
//                            }
//
//                            // 数据加载失败的回调，i 有三个类型
//                            // SocketNetError = 0;    socket链接失败
//                            // MemberInitError = 1;   会员初始化失败
//                            // MemberStop = 2;        会员账号暂停
//                            @Override
//                            public void onLoadError(int i) {
//
//                            }
//
//                            // 进入DG Activity的回调
//                            @Override
//                            public void onEnter() {
//
//                            }
//
//                            // 退出SDK的回调
//                            @Override
//                            public void onExit() {
//
//                            }
//
//                        }, 0);
//                    });
//                }
//                break;
//            case R.id.sicbo:
//                if (checkToken()) {
//                    if (!UIHelper.isSingle3()) {
//                        break;
//                    }
//                    // 之前分 DGLoadUtil与DGLoad 因为一个要回调一个不要回调，现在都要回调就合并成同一个了
//                    // DGLoadUtil.loadDGGameNoLoading();
//                    // DGLoadUtil.loadDGGame();
//                    // 该类为入口类，有两个方法，一个没有加载框，一个自带加载框
//                    // 参数都一样
//                    /*
//                     * context 当前activity的contenxt
//                     * token DG游戏token
//                     * gameId  之前是枚举类型，不可能有其他值，现在是int类型，要注意传入的值，如果不是龙虎和牛牛都显示百家乐
//                     * listener  DG游戏的回调
//                     * */
//                    runOnUiThread(() -> {
//                        // 之前分 DGLoadUtil与DGLoad 因为一个要回调一个不要回调，现在都要回调就合并成同一个了
//                        // DGLoadUtil.loadDGGameNoLoading();
//                        // DGLoadUtil.loadDGGame();
//                        // 该类为入口类，有两个方法，一个没有加载框，一个自带加载框
//                        // 参数都一样
//                        /*
//                         * context 当前activity的contenxt
//                         * token DG游戏token
//                         * gameId  之前是枚举类型，不可能有其他值，现在是int类型，要注意传入的值，如果不是龙虎和牛牛都显示百家乐
//                         * listener  DG游戏的回调
//                         * */
//                        DGLoadUtil.loadDGGame(DemoActivity.this, token, GameId.SICBO, getDomains(), new DGDataListener() {
//                            // 数据加载完成，还没有进入activity的回调
//                            @Override
//                            public void onLoadFinish() {
//
//                            }
//
//                            // 数据加载失败的回调，i 有三个类型
//                            // SocketNetError = 0;    socket链接失败
//                            // MemberInitError = 1;   会员初始化失败
//                            // MemberStop = 2;        会员账号暂停
//                            @Override
//                            public void onLoadError(int i) {
//
//                            }
//
//                            // 进入DG Activity的回调
//                            @Override
//                            public void onEnter() {
//
//                            }
//
//                            // 退出SDK的回调
//                            @Override
//                            public void onExit() {
//
//                            }
//
//                        }, 0);
//                    });
//                }
//                break;
//            case R.id.dragon:
//                if (checkToken()) {
//                    if (!UIHelper.isSingle3()) {
//                        break;
//                    }
//                    runOnUiThread(() -> {
//                        DGLoadUtil.loadDGGame(DemoActivity.this, token, GameId.DRAGON, getDomains(), new DGDataListener() {
//                            // 数据加载完成，还没有进入activity的回调
//                            @Override
//                            public void onLoadFinish() {
//                            }
//                            @Override
//                            public void onLoadError(int i) {
//
//                            }
//                            @Override
//                            public void onEnter() {
//                            }
//                            @Override
//                            public void onExit() {
//                            }
//
//                        }, 0);
//                    });
//                }
//                break;
//            case R.id.sedie:
//                if (checkToken()) {
//                    if (!UIHelper.isSingle3()) {
//                        break;
//                    }
//                    runOnUiThread(() -> {
//                        DGLoadUtil.loadDGGame(DemoActivity.this, token, GameId.SEDIE, getDomains(), new DGDataListener() {
//                            // 数据加载完成，还没有进入activity的回调
//                            @Override
//                            public void onLoadFinish() {
//                            }
//                            @Override
//                            public void onLoadError(int i) {
//
//                            }
//                            @Override
//                            public void onEnter() {
//                            }
//                            @Override
//                            public void onExit() {
//                            }
//
//                        }, 0);
//                    });
//                }
//                break;
            case R.id.token:
                getToken();
                break;
//            case R.id.cn:
//                AppVariable.changeLanguage(AppVariable.CN);
//                break;
//            case R.id.en:
//                AppVariable.changeLanguage(AppVariable.EN);
//                break;
//            case R.id.th:
//                AppVariable.changeLanguage(AppVariable.TH);
//                break;
//            case R.id.KO:
//                AppVariable.changeLanguage(AppVariable.KO);
//                break;
        }
    }

    private String getDomains() {
        String domains = "{" +
                "\"file\":\"https://dg-asia.aogebgjj.com/\", " +
                "\"login\":\"https://login.ruihejade.com/apidata/\"," +
                "\"bgmVersion\":\"2\" }";
        return domains;
    }

    private boolean checkToken() {
        if (token == null) {
            Toast.makeText(getBaseContext(), "请获得token", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void getToken() {
        EditText usernameEdt = (EditText) findViewById(R.id.username);
        EditText pwdEdt = (EditText) findViewById(R.id.pwd);
        new Thread(() -> {
            final String jsonString = HttpUtil.sendPost(gameToken_URL, "");
            Log.e("token = ", jsonString);
            runOnUiThread(() -> {
                try {
                    JSONObject myJsonObject = new JSONObject(jsonString);
                    token = myJsonObject.get("token").toString();//"TEST000003"; //CNY00003
                    Toast.makeText(DemoActivity.this, "获取 token 成功:" + token, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getBaseContext(), jsonString, Toast.LENGTH_SHORT).show();
                }
            });
        }).start();
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setToken(String token) {
        this.token = token;
    }

}
