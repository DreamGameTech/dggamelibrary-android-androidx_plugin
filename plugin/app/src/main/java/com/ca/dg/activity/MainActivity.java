package com.ca.dg.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

//import com.ca.dg.app.AppVariable;
//import com.ca.dg.constant.GameId;
//import com.ca.dg.app.AppVariable;
//import com.ca.dg.biz.MusicServiceBiz;
import com.ca.dg.constant.GameId;
//import com.ca.dg.service.SoundManager;
import com.qihoo360.replugin.sample.demo1.R;

import org.json.JSONObject;
//import com.tencent.demo.util.UIHelper;

public class MainActivity extends Activity implements View.OnClickListener {

    private String token = null;

    private static String TAG = "MainActivity";

//    String gameToken_URL =  "https://www.20266666.com/game/h5";
//            :"http://www.dg99a.com/game/h5";// 测试
    String gameToken_URL = "http://www.dg99a.com/game/h5";
//    String gameToken_URL = "https://login.ruihejade.com/apidata/game/h5";

    private Button button;
    private Button dragon;
    private Button bull;
    private Button roulette;
    private Button sicbo;
    private Button threecard;
    private Button tokenBt;
    private Button sedieBt;
    private Button musicBt;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_waike3);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);

        dragon = (Button) findViewById(R.id.bt2);
        dragon.setOnClickListener(this);

        bull = (Button) findViewById(R.id.bt3);
        bull.setOnClickListener(this);

        roulette = (Button) findViewById(R.id.bt4);
        roulette.setOnClickListener(this);

        sicbo = (Button) findViewById(R.id.bt5);
        sicbo.setOnClickListener(this);

        sedieBt = (Button) findViewById(R.id.sedie);
        sedieBt.setOnClickListener(this);

        tokenBt = (Button) findViewById(R.id.token);
        tokenBt.setOnClickListener(this);

        musicBt = (Button) findViewById(R.id.music);
        musicBt.setOnClickListener(this);

        intent = new Intent(this, BridgeActivity.class);
    }

    private String getDomains() {
        // 正式  这个文件属性 登录的时候可以获取到。
//        String domains = "{" +
//                "\"file\":\"https://dg-asia.ruihejade.com/\", " +
//                "\"login\":\"https://login.ruihejade.com/apidata/\"," +
//                "\"bgmVersion\":\"2\" }";

        String domains = "{" +"back_appvideo: \"rtmp://play-ct.dg0.co/vod/mp4#rtmp://play-ct.dg0.co:1936/vod/mp4#rtmp://playback.nanhai66.com/vod/mp4\"\n," +
                "back_img: \"https://tupian-dg.dg0.co/#https://tupian-ct.dg0.co/#https://tupian.nanhai666.com/image/#https://tupian-cp.dg0.co/\"\n," +
                "back_video: \"https://videoback-dg.dg0.co/#https://videoback-ct.dg0.co/#https://playback.nanhai66.com/#https://videoback-cp.dg0.co/\"\n," +
                "bgmVersion: \"2\"\n," +
                "file: \"https://dg-asia.ruihejade.com/\"\n," +
                "freeToken: \"http://www.dg99a.com/game/h5\"\n," +
                "login: \"http://www.dg99a.com/\"\n," +
                "tcp: \"172.16.10.55:8860\" }";

        return domains;
    }

    @Override
    public void onClick(View v) {
//        if (!UIHelper.isSingle3()) {// 注意不要同时点击两个游戏， 不然 有一个游戏会卡住。
//            return;
//        }  //TODO
//        AppVariable.language = AppVariable.CN;// 要修改语言，在进入游戏之前修改
        if (v.getId() != R.id.token) {
            if (!checkToken()) {
                return;
            }
        }
//        DGLoadUtil.setBarrageHide();// 设置弹幕与聊天隐藏与否， 默认 不隐藏

        intent.putExtra("minBet", 10);
        intent.putExtra("token", token);
        intent.putExtra("domains", getDomains());
        intent.putExtra("hideBarrage", false);

        final int req = 105;

        switch (v.getId()) {
            case R.id.button:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        intent.putExtra("gameId", GameId.BACCARAT);
                        startActivityForResult(intent, req);
                    }
                });
                break;
            case R.id.bt2:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        intent.putExtra("gameId", GameId.DRAGON);
                        startActivityForResult(intent, req);
                    }
                });
                break;
            case R.id.bt3:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        intent.putExtra("gameId", GameId.BULL);
                        startActivityForResult(intent, req);
                    }
                });
                break;
            case R.id.bt4:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        intent.putExtra("gameId", GameId.ROULETTE);
                        startActivityForResult(intent, req);
                    }
                });
                break;
            case R.id.sedie:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        intent.putExtra("gameId", GameId.SEDIE);
                        startActivityForResult(intent, req);
                    }
                });
                break;
            case R.id.bt5:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        intent.putExtra("gameId", GameId.SICBO);
                        startActivityForResult(intent, req);
                    }
                });
                break;
            case R.id.music:
//                SoundManager.getInstance(this.getBaseContext());
//                AppVariable.initUserInfo(this.getBaseContext());
//                MusicServiceBiz.startStateSound(SoundManager.START_BET,  getBaseContext(), true);
                break;
            case R.id.token:
                getToken();
                break;
        }
    }

    private boolean checkToken() {

        if (token == null) {
            Toast.makeText(MainActivity.this, "请获得token", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void getToken() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String jsonString = HttpUtil.sendPost(gameToken_URL, "");
                Log.e("token = ", jsonString);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject myJsonObject = new JSONObject(jsonString);
                            token = myJsonObject.get("token").toString();//"TEST000003"; //CNY00003
                            Toast.makeText(MainActivity.this, "获取 token 成功:" + token, Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, jsonString, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).start();
    }
}

