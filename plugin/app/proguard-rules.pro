# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\tools\eclipse\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html
#
# Add any project specific keep options here:
-optimizationpasses 5   #指定执行几次优化 默认执行一次
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-ignorewarnings
-allowaccessmodification
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

-obfuscationdictionary dic.txt
-packageobfuscationdictionary dic.txt

-keepattributes LineNumberTable,SourceFile
#-keep com.opensource.svgaplayer.**{ *; }

-keep class com.squareup.wire.** { *; }
-keep class com.opensource.svgaplayer.proto.** { *; }

-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
    public void get*(...);
}

-keep class cn.nodemedia.** { *; }

-keepclasseswithmembernames class * {     # 保持 native 方法不被混淆
    native <methods>;
}
-keep public class * implements android.os.Parcelable

-keep public class * implements java.io.Serializable

-dontwarn android.databinding.**
-keep class android.databinding.** { *; }

-keep public class com.ca.dg.R$*{
    public static final int *;
}

-keepattributes *Annotation*
-keepattributes Signature,InnerClasses
-keepclasseswithmembers class io.netty.** { *; }
-keepnames class io.netty.** { *; }

-keep class com.google.protobuf.** { *; }
-keep interface com.google.protobuf.** { *; }

-dontwarn com.alibaba.fastjson.**
-keep class com.alibaba.fastjson.** { *; }
-keep interface com.alibaba.fastjson.** { *; }

-keep class com.pili.pldroid.** { *; }
-keep interface com.pili.pldroid.** { *; }
-keep class tv.danmaku.** { *; }

-keepclassmembers public class fqcn.of.javascript.interface.for.webview {
   public *;
}

-keep public class com.ca.dg.model.** { *; }
-keep public class com.ca.dg.viewModel.** { *; }
-keep public class com.ca.dg.view.custom.bet.ImgsLocal { *; }

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}

-keep public class com.ca.dg.R$*{
public static final int *;
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

#阿里百川热更新
-keep class * extends java.lang.annotation.Annotation

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keepnames class * implements java.io.Serializable

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    !private <fields>;
    !private <methods>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keep public class com.ca.dg.constant.GameId { *; }
-keep public class com.ca.dg.activity.BridgeActivity { *; }

-keep class   com.ca.dg.util.UIHelper {
   public <methods>;
 }

-keep class com.ca.dg.activity.DGLoadUtil {
    public <methods>;
}

# 数据绑定
-keep class com.ca.dg.DataBindingInfo
-keep class * extends androidx.databinding.** { *; }

#弹幕
-keep class com.orient.tea.barragephoto.** { *; }
-keep interface com.orient.tea.barragephoto.** { *; }

# AndroidX

-keep class com.google.android.material.** {*;}
-keep class androidx.** {*;}
#-keep public class * extends androidx.**

-keep interface androidx.** {*;}
-dontwarn com.google.android.material.**
-dontnote com.google.android.material.**
-dontwarn androidx.**

######
#必须要开 否则ele的activity混淆会失效
-dontshrink
-dontskipnonpubliclibraryclassmembers
-dontoptimize
#-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-keepattributes Exceptions
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

#关闭警告
-dontwarn android.support.**
#webview 使用了要加上
-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

-keepclassmembers,allowoptimization enum * {
      public static **[] values();
      public static ** valueOf(java.lang.String);
}
-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}
#移除日志
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}
-assumenosideeffects class java.io.PrintStream {
    public *** println(...);
    public *** print(...);
}

-keep class com.ca.dg.util.* { *; }
-keep interface com.ca.dg.util.** { *; }

-keep class com.qihoo360.replugin.** { *; }
-keep interface com.qihoo360.replugin.** { *; }