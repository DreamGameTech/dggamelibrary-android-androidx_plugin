package com.ca.dg.host;

import android.app.Application;
import android.content.SharedPreferences;

public class ShareUtil {

    private ShareUtil(Application app) {
        ShareUtil.app = app;
        shareUtil = this;
    }

    public static ShareUtil instance(Application application) {
        if (shareUtil == null) {
            shareUtil = new ShareUtil(application);// 现在需要处理下这里
        }
        return shareUtil;
    }

    public static ShareUtil shareUtil;

    public static Application app;
    /*背景音乐先不管， 将其它的页面给撑起来*/

    private static SharedPreferences userShare;

    public static final String SHARE_NAME = "user_info";

    public static void put(String key, Object value) {
        if (userShare == null) {
            userShare = app.getSharedPreferences(SHARE_NAME, Application.MODE_PRIVATE);
        }
        SharedPreferences.Editor editor = userShare.edit();
        if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        }
        editor.commit();
    }

    public static <T> T get(String key, Object defValue) {
        if (userShare == null) {
            userShare = app.getSharedPreferences(SHARE_NAME, Application.MODE_PRIVATE);
        }
        if (defValue instanceof String) {
            return (T) userShare.getString(key, (String) defValue);
        } else if (defValue instanceof Integer) {
            return (T) Integer.valueOf(userShare.getInt(key, (Integer) defValue));
        } else if (defValue instanceof Boolean) {
            return (T) Boolean.valueOf(userShare.getBoolean(key, (Boolean) defValue));
        } else if (defValue instanceof Long) {
            return (T) Long.valueOf(userShare.getLong(key, (Long) defValue));
        } else if (defValue instanceof Float) {
            return (T) Float.valueOf(userShare.getFloat(key, (Float) defValue));
        }
        return null;
    }

}
