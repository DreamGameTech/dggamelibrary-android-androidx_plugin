package com.ca.dg.host;

import com.ca.dg.host.util.AESHelper;
import com.ca.dg.host.util.MD5;
import java.io.File;

/**
 * 描述：
 * 打包出来的apk , 可以使用这个类计算md5 值 及加密。 用于 host 项目下载 解密 加载
 */
public class HostUtil {

    public static void main(String... a) {

        // android studio 执行main 方法的三个设置
//        https://www.jianshu.com/p/38e86e32cf67

        //1  需要计算出文件的md5 值
        //2需要对文件进行加密处理， 并进行重命名
        //3 需要验证 加密及解密后的 md5 值
        String sourcePath = "/Users/Patton/Documents/pro/dev/androidx_plugin/plugin/app/release/app-release.apk";

        String descPath = "/Users/Patton/Documents/pro/dev/androidx_plugin/plugin/app/release/dgPlugin2";

        String decryPath = "/Users/Patton/Documents/pro/dev/androidx_plugin/plugin/app/release/dgPlugin2-decry.apk";

        System.err.println("原文件的Md5值 " + MD5.getFileMD5(sourcePath));

        new File(descPath).delete();
        boolean encryFlg = AESHelper.encryptFile("0q33k1ngpzxlp70n", sourcePath, descPath);

        System.out.println("加密成功" + encryFlg);
        System.out.println("加密后的Md5 " + MD5.getFileMD5(descPath));

        new File(decryPath).delete();
        boolean decryFlg = AESHelper.decryptFile("0q33k1ngpzxlp70n", descPath, decryPath);

        System.out.println("解密" + decryFlg);
        System.err.println("解密后文件md5值 " + MD5.getFileMD5(decryPath));
    }
}
