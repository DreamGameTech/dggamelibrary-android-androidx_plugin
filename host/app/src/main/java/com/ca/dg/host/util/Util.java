package com.ca.dg.host.util;

public class Util {

    private static final int MIN_DELAY_TIME = 2000;  // 两次点击间隔不能少于1000ms

    private static long lastClickTime;

    public static synchronized boolean isFastClick() {
        boolean flag = true;
        long currentClickTime = System.currentTimeMillis();
        if ((currentClickTime - lastClickTime) >= MIN_DELAY_TIME) {
            lastClickTime = currentClickTime;
            flag = false;
        }
        return flag;
    }

    public static synchronized boolean isFastClick(int time) {
        boolean flag = true;
        long currentClickTime = System.currentTimeMillis();
        if ((currentClickTime - lastClickTime) >= time) {
            lastClickTime = currentClickTime;
            flag = false;
        }
        return flag;
    }
}
