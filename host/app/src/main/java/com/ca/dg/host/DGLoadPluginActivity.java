package com.ca.dg.host;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.ca.dg.host.bean.PluginConstant;
import com.ca.dg.host.util.AESHelper;
import com.ca.dg.host.util.LoginUtil;
import com.ca.dg.host.util.MD5;
import com.ca.dg.host.util.Util;
import com.ca.dg.host.util.WeakHandler;
import com.qihoo360.replugin.RePlugin;
import com.qihoo360.replugin.model.PluginInfo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class DGLoadPluginActivity extends Activity {

    private String TAG = "DGLoadPluginActivity";

    private TextView versionTv;

    private boolean isFront = false;

    private boolean clickeAble = true;

    ProgressBar progress;

    private TextView statuTv;

    private final static int progress_rate = 1;

    private final static int load_state = 2;

    private final static int load_plugin = 5;

    private final static int down_load_ok = 4;

    private boolean isReady = false;

    private int gameId = 1;

    Button state = null;

    Button start = null;

    private boolean clickable = true;

    boolean canDownload = false;

    private WeakHandler handler = new WeakHandler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == progress_rate) {
                int result = (int) msg.obj;
                progress.setProgress(result);
            } else if (msg.what == load_state) {
                statuTv.setText(String.valueOf(msg.obj));
            } else if (msg.what == 3) {
                getVersion(null);
            } else if (msg.what == down_load_ok) {
                if (isFront) {
                    encryFile();
                } else {
                    // 如果不可见， 那么当可见的时候再执行安装
                    isReady = true;
                    clickeAble = false;
                }
            } else if (msg.what == load_plugin) {
                statuTv.setText("loading...");
                installPlugin(new File(pluginDecFilePath));
            }
            return false;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plugin_load);
        progress = (ProgressBar) findViewById(R.id.progressBar);
        statuTv = (TextView) findViewById(R.id.tips);

        Intent gameIntent = getIntent();
        gameId = gameIntent.getIntExtra("gameId", 1);
        start = (Button) findViewById(R.id.bt_download);


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Util.isFastClick()) {
                    return;
                }
                if (RePlugin.isPluginInstalled(PluginConstant.pluginAlias)) {
                    start.setText("start");
                    startGame();
                } else {
                    if (canDownload && clickable) {
                        clickable = false;
                        statuTv.setText("downloading...");
                        downLoad();
                    }
                }
            }
        });

        findViewById(R.id.bt_uninstall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (RePlugin.isPluginInstalled(PluginConstant.pluginAlias)) {
                    boolean flg = RePlugin.uninstall(PluginConstant.pluginAlias);
                    PluginConstant.getInstance().setPluginNeedStart();
                }
            }
        });

        state = (Button) findViewById(R.id.bt_state);

        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setState();
            }
        });

        boolean pluginInstalled = RePlugin.isPluginInstalled(PluginConstant.pluginAlias);
        if (!pluginInstalled) {
            // token 5分钟 有效， demo 提前获取了
            LoginUtil.getToken(DGLoadPluginActivity.this);
            downLoad();
        } else {
            startGame();
        }
    }

    private void setState() {
        boolean flg = PluginConstant.getInstance().isPluginNeedStart();
        if (RePlugin.isPluginInstalled(PluginConstant.pluginAlias)) {
            state.setText("Installed" + (flg ? " need restart" : ""));
            start.setText("start");
        } else {
            state.setText("Not Installed" + (flg ? " need restart" : ""));
        }
    }

    private void uninstall() {
        if (RePlugin.isPluginInstalled(PluginConstant.pluginAlias)) {
            boolean flg = RePlugin.uninstall(PluginConstant.pluginAlias);
            PluginConstant.getInstance().setPluginNeedStart();
        }
    }

    public void downLoadPlugin(View v) {
        if (!clickeAble) {
            return;
        }
        downLoad();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isFront = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isFront = false;
        if (isReady) {
            isReady = false;
            encryFile();
        }
    }

    private String pluginLocalPath = "";

    private String pluginDecFilePath = "";

    /*
     * 这里下载的是plugin 打出来打apk文件, 再经过 HostUtil 加密后 形成的文件 dgPlugin2
     * */
    public void downLoad() {
        clickeAble = false;
        pluginLocalPath = getFilesDir().getAbsolutePath() + File.separator + PluginConstant.pluginAlias;
        String url = "http://x.x.x.x:8000/dgPlugin2";

        ShareUtil.put(PluginConstant.plugin_local_path, pluginLocalPath);
        ShareUtil.put(PluginConstant.plugin_url, url);

        ShareUtil.put(PluginConstant.plugin_md5, "ca24a09d8f5b5987546b222fb8ec5af1");

        boolean exists = new File(pluginLocalPath).exists();

        if (exists) {
            String md5 = ShareUtil.get(PluginConstant.plugin_md5, "ca24a09d8f5b5987546b222fb8ec5af1");
            String fileMD5 = MD5.getFileMD5(pluginLocalPath);
            if (md5.equals(fileMD5)) {
                Toast.makeText(this, "file exist", Toast.LENGTH_SHORT).show();
                Message stateMsg = Message.obtain();
                stateMsg.what = down_load_ok;
                handler.sendMessage(stateMsg);
                clickeAble = true;
                return;
            }
            deleteFile();
        }
        downPlugin(url);
    }

    private void encryFile() {

        pluginDecFilePath = getFilesDir().getAbsolutePath() + File.separator + PluginConstant.pluginAlias + ".apk";
        Message stateMsg = Message.obtain();
        boolean decryptFlg = AESHelper.decryptFile("0q33k1ngpzxlp70n",
                pluginLocalPath,
                pluginDecFilePath
        );

        if (decryptFlg) {

            stateMsg = Message.obtain();
            stateMsg.what = load_state;
            stateMsg.obj = "load plugin...";
            handler.sendMessage(stateMsg);
            handler.sendEmptyMessage(load_plugin);

        } else {
            stateMsg = Message.obtain();
            stateMsg.what = load_state;
            stateMsg.obj = "install fail...";
            handler.sendMessage(stateMsg);
        }
    }

    private void installPlugin(File file) {
        MyTask mTask = new MyTask();
        mTask.execute(file);
    }

    private class MyTask extends AsyncTask<File, Integer, Boolean> {

        // 方法1：onPreExecute（）
        // 作用：执行 线程任务前的操作
        @Override
        protected void onPreExecute() {
            statuTv.setText("loading...");
            // 执行前显示提示
        }

        // 方法2：doInBackground（）
        // 作用：接收输入参数、执行任务中的耗时操作、返回 线程任务执行的结果
        // 此处通过计算从而模拟“加载进度”的情况
        @Override
        protected Boolean doInBackground(File... params) {
            PluginInfo info = RePlugin.install(params[0].getAbsolutePath());
            if (info != null) {
                boolean preloadFlg = RePlugin.preload(info);
                if (preloadFlg) {
                    //  这里开始调用进入游戏
                    //  根据点击的游戏信息进行加载
                    return true;
                } else {
                    Log.i("plugin", "load plugin fail");
                    uninstall( );
                }
            }
            return false;
        }

        // 方法4：onPostExecute（）
        // 作用：接收线程任务执行结果、将执行结果显示到UI组件
        @Override
        protected void onPostExecute(Boolean result) {
            // 执行完毕后，则更新UI
            if (result != null) {
                if (result.booleanValue()) {
                    startGame();
                } else {
                    Toast.makeText(DGLoadPluginActivity.this, "load game fial", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    }

    private void startGame() {
        Intent intent = new Intent();

        intent.putExtra("token", LoginUtil.token);
        intent.putExtra("domains", LoginUtil.getDomains());
        intent.putExtra("hideBarrage", false);
        intent.putExtra("gameId", gameId);

        // 在插件的 AndroidManifest.xml 里定义了别名， 这里必须以别名的方式启动
        if (!PluginConstant.getInstance().isPluginNeedStart()) {
            boolean startFlg = RePlugin.startActivityForResult(this, intent.setComponent(new ComponentName(PluginConstant.pluginAlias, "com.ca.dg.activity.BridgeActivity")), REQUEST_CODE_DEMO5);
            if (!startFlg) {
                Toast.makeText(DGLoadPluginActivity.this, "Failed to enter the game", Toast.LENGTH_SHORT).show();
                boolean flg = RePlugin.uninstall(PluginConstant.pluginAlias);
                PluginConstant.getInstance().setPluginNeedStart();
                finish();
            }
        } else {
            Toast.makeText(DGLoadPluginActivity.this, "app need restart", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteFile() {
        String sourcePath = ShareUtil.get(PluginConstant.plugin_local_path, "");
        File sourceFile = new File(sourcePath);
        if (sourceFile.exists()) {
            sourceFile.delete();
        }

        String descPath = getFilesDir().getAbsolutePath() + File.separator + PluginConstant.pluginAlias + ".apk";
        File descFile = new File(descPath);
        if (descFile.exists()) {
            descFile.delete();
        }
    }

    public int getVersion(View v) {
        int pluginVersion = RePlugin.getPluginVersion(PluginConstant.pluginAlias);
        versionTv.setText("plugin version:" + pluginVersion);
        return pluginVersion;
    }

    private static final int REQUEST_CODE_DEMO5 = 0x011;
    private static final int RESULT_CODE_DEMO5 = 0x012;

    /**
     * 这里是游戏返回的时的参数
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_DEMO5 && resultCode == RESULT_CODE_DEMO5) {
            // 这里主要用来模拟 获取返回的参数
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    Object value = bundle.get(key);
                    Log.i(TAG, String.format("%s %s (%s)", key,
                            value.toString(), value.getClass().getName()));
                }
            }
            finish();
        }
    }

    public void exit(View v) {
        ActivityManager mActivityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> mList = mActivityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : mList) {
            if (runningAppProcessInfo.pid != android.os.Process.myPid()) {
                android.os.Process.killProcess(runningAppProcessInfo.pid);
            }
        }
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

    private static final int REQUEST_EXTERNAL_STORAGE = 1;

    private static String[] PERMISSIONS_STORAGE = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE"};

    public static void verifyStoragePermissions(Activity activity) {

        try {
            //检测是否有写的权限
            int permission = ActivityCompat.checkSelfPermission(activity,
                    "android.permission.WRITE_EXTERNAL_STORAGE");
            if (permission != PackageManager.PERMISSION_GRANTED) {
                // 没有写的权限，去申请写的权限，会弹出对话框
                ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void downPlugin(final String downUrl) {

        Thread download = new Thread() {
            @Override
            public void run() {
                FileOutputStream fos = null;
                InputStream is = null;

                boolean isDownComplete = false;
                try {

                    Message stateMsg = Message.obtain();
                    stateMsg.what = load_state;
                    stateMsg.obj = "downloading";
                    handler.sendMessage(stateMsg);

                    URL url = new URL(downUrl);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    int fileLenth = connection.getContentLength();

                    String path = getFilesDir().getAbsolutePath() + File.separator + PluginConstant.pluginAlias;
                    ShareUtil.put(PluginConstant.plugin_local_path, path);

                    File apkFile = new File(path);

                    fos = new FileOutputStream(apkFile);
                    is = connection.getInputStream();
                    byte[] buf = new byte[1024];
                    int len = 0;
                    float count = 0;
                    boolean isCancel = false;
                    while (!isCancel) {
                        if ((len = is.read(buf)) > 0) {
                            count += len;
                            fos.write(buf, 0, len);
                            fos.flush();

                            Message m = Message.obtain();
                            m.what = 1;
                            m.obj = (int) (count * 100L / fileLenth);
                            handler.sendMessage(m);
                        } else {
                            isCancel = true;
                        }
                    }
                    if (len == -1) {
                        isDownComplete = true;
                        Log.i("jetty", "down load ok");
                        runOnUiThread(new Runnable() {
                                          @Override
                                          public void run() {
                                              Toast.makeText(DGLoadPluginActivity.this, "download success", Toast.LENGTH_LONG).show();
                                          }
                                      }
                        );
                    }
                } catch (Exception e) {
                    Log.i("jetty", "exception" + e.getMessage());
                    e.printStackTrace();
                    isDownComplete = false;
                    Message stateMsg = Message.obtain();
                    stateMsg.what = load_state;
                    stateMsg.obj = "download fail";
                    handler.sendMessage(stateMsg);

                } finally {
                    try {
                        if (is != null) {
                            is.close();
                        }
                        if (fos != null) {
                            fos.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (isDownComplete) {
                        handler.sendEmptyMessage(down_load_ok);
                        clickeAble = true;
                    }
                }
            }
        };
        download.start();
    }
}


