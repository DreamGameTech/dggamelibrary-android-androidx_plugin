package com.ca.dg.host.util;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class LoginUtil {

    /**
     * 这个值 在获取token 的时候可以拿到， 别用这个写死的
     *
     * @return
     */
    public static String getDomains() {
//        String domains = "{\n" +
//                "\"file\": \"https://dg-asia.ruihejade.com/\"," +
//                "\"tcp\": \"appa.gztsshuju.com:8860\"," +
//                "\"game_wss\": \"wss://appa.gztsshuju.com:8871\"," +
//                "\"freeToken\":\"https://login.ruihejade.com/apidata/game/h5/\"," +
//                "\"login\": \"https://login.ruihejade.com/apidata/\"," +
//                "\"chat_wss\": \"wss://appa.gztsshuju.com:8873\"," +
//                "\"back_img\": \"https://tupian-dg.dg0.co/#https://tupian-ct.dg0.co/#https://tupian.nanhai666.com/image/#https://tupian-cp.dg0.co/\"," +
//                "\"back_video\": \"https://videoback-dg.dg0.co/#https://videoback-ct.dg0.co/#https://playback.nanhai66.com/#https://videoback-cp.dg0.co/\"," +
//                "\"back_appvideo\": \"rtmp://play-ct.dg0.co/vod/mp4#rtmp://play-ct.dg0.co:1936/vod/mp4#rtmp://playback.nanhai66.com/vod/mp4\"," +
//                "\"dealerFile\": \"https://dg-asia.ruihejade.com/#https://dg-asia.ruihejade.com/#https://nh-asia.17xiangqu.com/\"," +
//                "\"bgmVersion\": \"2\"," +
//                "\"bobingrobot\": \"https://bobingrobot.dg75l.com/ranking\"" +
//                "}";

        String domains = "{" +"back_appvideo: \"rtmp://play-ct.dg0.co/vod/mp4#rtmp://play-ct.dg0.co:1936/vod/mp4#rtmp://playback.nanhai66.com/vod/mp4\"\n," +
                "back_img: \"https://tupian-dg.dg0.co/#https://tupian-ct.dg0.co/#https://tupian.nanhai666.com/image/#https://tupian-cp.dg0.co/\"\n," +
                "back_video: \"https://videoback-dg.dg0.co/#https://videoback-ct.dg0.co/#https://playback.nanhai66.com/#https://videoback-cp.dg0.co/\"\n," +
                "bgmVersion: \"2\"\n," +
                "file: \"https://dg-asia.ruihejade.com/\"\n," +
                "freeToken: \"http://www.dg99a.com/game/h5\"\n," +
                "login: \"http://www.dg99a.com/\"\n," +
                "tcp: \"172.16.10.55:8860\" }";

        return domains;
    }

    public static void getToken(final Activity context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String jsonString = HttpUtil.sendPost(gameToken_URL, "");
                Log.e("token = ", jsonString);
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject myJsonObject = JSON.parseObject(jsonString);
                            token = myJsonObject.get("token").toString();//"TEST000003"; //CNY00003
                            Toast.makeText(context, "获取 token 成功:" + token, Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, jsonString, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).start();
    }

    //    private static String gameToken_URL = "https://www.20266666.com/game/h5";
    private static String gameToken_URL = "http://www.dg99a.com/game/h5";// 测试

    public static String token;
}
